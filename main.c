/*
 * Describe software here
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <math.h>
#include "arduino-serial-lib.h"
#include "gsl/statistics/gsl_statistics_double.h"

#define BAUDRATE 9600
#define _POSIX_SOURCE 1 /* POSIX compliant source */
#define DEFAULT_DEVICE "/dev/tty.usbmodemFA131"
#define BUFFER_SIZE 256
#define MAX_TIMEOUT 500 // milisseconds
#define SAMPLES 50
#define FILENAME "measures.csv"

double sensor_calibrate(int fd);
double sensor_get_measure(int fd);
double sensor_measure(int fd, double offset);

int main(int argc, char *argv[])
{
  int fd, k=0, i;
  FILE *fmeasures;
  char *dev = NULL;
  double mag_offset = 0;
  double *measures, tmp = 0;
  char opt = 0;

  switch(argc) {
  case 2:
    dev = strdup(argv[1]);
    break;
  default:
    dev = strdup(DEFAULT_DEVICE);
    break;
  }

  measures = (double *)malloc(sizeof(double)); 
  
  if (dev == NULL) {
    printf("Error reading device name %s\n", dev);
    return -1;
  }
  printf("Starting serial communication with device %s ...\n", dev);

  if ( (fd = serialport_init(dev, BAUDRATE)) < 0) {
    printf("Error opening serial.\n");
    return -1;
  }
  
  while(opt != 'e') {
    printf("Choose your option:\n");
    printf("\t\tc = Let the sensor far from magnetic fields and calibrate it.\n");
    printf("\t\tm = Put the sensor where you want to measure the point %d and measure it.\n", k+1);
    printf("\t\tp = print measures to %s.\n", FILENAME);
    printf("\t\te = Exit software.\n");
    printf("\t\t\tChoose: ");
    opt = getchar();
    switch(opt) {
    case 'c':
      mag_offset = sensor_calibrate(fd);
      break;
    case 'm':
      if ((tmp = sensor_measure(fd, mag_offset)) == NAN) {
	printf("Some error occured during the measure process... Please restart the program.\n");
      }
      k++;
      measures = (double *)realloc(measures, k * sizeof(double));
      measures[k-1] = tmp;
      break;
    case 'p':
      fmeasures = fopen(FILENAME, "w");
      fprintf(fmeasures, "Point, Measure B (mT)\n");
      for (i = 0; i < k; i++) {
	fprintf(fmeasures, "%d, %f\n", i+1, measures[i]);
      }
      fclose(fmeasures);
      break;
    case 'e':
      break;
    default:
      break;
    }
  }

  serialport_flush(fd);
  if (serialport_close(fd) < 0) {
    printf("Error closing serial...\n");
  }
  free(measures);
  printf("\n\nFinishing the software.\n");
  return 0;
}

double sensor_calibrate(int fd) {
  int i;
  double samples[SAMPLES];
  double offset, offset_sd;
  printf("\n\nPress enter to start calibration procedure:\n");
  getchar();
  printf("starting...\n\n");
  for (i = 0; i < SAMPLES; i++) { 
    samples[i] = sensor_get_measure(fd);
    if (samples[i] == NAN) {
      printf("Pay attention because sample %d was changed to %d due to serial misreading.\n", i, i-1);
      if (i == 0) {
	printf("It was not possible to perform the calibration.\n");
	return NAN;
      }
      samples[i] = samples[i-1];
    }
  }

  offset = gsl_stats_mean(samples, 1, SAMPLES);
  offset_sd = gsl_stats_sd_m(samples, 1, SAMPLES, offset);

  printf("The calculated offset of calibration is %.3f mT with %f os Standard Deviation", offset, offset_sd);
  
  return offset;
}

double sensor_get_measure(int fd) {
  double measure = 0;
  int last = 0;
  char buf[BUFFER_SIZE] = "";
  last = serialport_read_until(fd, buf, '\n', BUFFER_SIZE, MAX_TIMEOUT);
  if (last < 0) {
    return NAN;
  }
  buf[last-1] = '\n';
  //printf("[DEBUG] Measure: %s\n", buf);
  measure = atof(buf);
  return measure;
}

double sensor_measure(int fd, double offset) {
  int i;
  double samples[SAMPLES];
  double measure, measure_sd;
  printf("\n\n\nPress enter to start the measurement:\n");
  getchar();
  printf("starting...\n\n");
  for (i = 0; i < SAMPLES; i++) { 
    samples[i] = sensor_get_measure(fd);
    if (samples[i] == NAN) {
      printf("Pay attention because sample %d was changed to %d due to serial misreading.\n", i, i-1);
      if (i == 0) {
	printf("It was not possible to perform the measurement.\n");
	return NAN;
      }
      samples[i] = samples[i-1];
    }
  }
  measure = gsl_stats_mean(samples, 1, SAMPLES);
  measure_sd = gsl_stats_sd_m(samples, 1, SAMPLES, measure);
  measure -= offset;
  printf("The calculated measure is %.3f mT with %f os Standard Deviation", measure, measure_sd);
  
  return measure;
}
