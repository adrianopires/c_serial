CCOMPILER=gcc
CFLAGS= -Wall -I${CURDIR}/gsl
#LFLAGS= -L${CURDIR}/gsl/.libs/ -lgsl -L${CURDIR}/gsl/cblas/.libs -lgslcblas
LFLAGS= ${CURDIR}/gsl/.libs/libgsl.a ${CURDIR}/gsl/cblas/.libs/libgslcblas.a
OBJECTS=main.o arduino-serial-lib.o
PROGRAM_NAME=serial

all: ${OBJECTS}
	${CCOMPILER} ${CFLAGS} ${OBJECTS} -o ${PROGRAM_NAME} ${LFLAGS}

clean:
	rm -rf *~ \#* *# *.o *.a
